package tests;

import lib.CoreTestCaseForTwoSessions;
import lib.ui.*;
import lib.ui.factories.*;
import org.junit.Test;

public class TwoSessionsTests extends CoreTestCaseForTwoSessions {

    @Test
    public void test()
    {
        MessagesPageObject MessagesPageObject = MessagesPageObjectFactory.get(driver);
        MessagesPageObject.confirmAlerts();
        MessagesPageObject MessagesPageObject_Second = MessagesPageObjectFactory.get(driver_second);
        MessagesPageObject_Second.confirmAlerts();
        CommonNavigationUI CommonNavigationUI = CommonNavigationUiFactory.get(driver);
        CommonNavigationUI.switchToContactsTab();
        CommonNavigationUI CommonNavigationUI_Second = CommonNavigationUiFactory.get(driver_second);
        CommonNavigationUI_Second.switchToEditProfileScreen();
        ContactsMainPageObject ContactsMainPageObject = ContactsMainPageObjectFactory.get(driver);
        EditContactPageObject EditContactPageObject_Second = EditContactPageObjectFactory.get(driver_second);
        ContactsMainPageObject.clickOnTheAddContactButton();
        EditContactPageObject_Second.showMyContactClick();
        ContactsMainPageObject.confirmCameraAlertAndAddContact();
        ContactsMainPageObject.giveNameToContact();
        ContactsMainPageObject.doneButtonClick();
        EditContactPageObject_Second.AllKeysReadButtonClick();
        ContactsInfoPageObject ContactsInfoPageObject = ContactsInfoPageObjectFactory.get(driver);
        ContactsInfoPageObject.sendMessageButtonClick();
        P2pChatViewControllerPageObject P2pChatViewControllerPageObject =
                P2pChatViewControllerPageObjectFactory.get(driver);
        P2pChatViewControllerPageObject.enterNewMessage("Привет!");
        P2pChatViewControllerPageObject.sendMessageButtonClick();
        CommonNavigationUI_Second.switchToMessagesTab();
        MessagesPageObject_Second.goToChat();
        P2pChatViewControllerPageObject P2pChatViewControllerPageObject_Second =
                P2pChatViewControllerPageObjectFactory.get(driver_second);
        P2pChatViewControllerPageObject_Second.checkMessageText("id:Привет!");
        P2pChatViewControllerPageObject_Second.enterNewMessage("Hello...");
        P2pChatViewControllerPageObject_Second.sendMessageButtonClick();
        P2pChatViewControllerPageObject.checkMessageText("id:Hello...");
        P2pChatViewControllerPageObject.enterNewMessage(
                "[XCUITest] Not clearing log files. Use `clearSystemFiles` capability to turn on.");
        P2pChatViewControllerPageObject.sendMessageButtonClick();
        P2pChatViewControllerPageObject_Second.checkMessageText(
                "id:[XCUITest] Not clearing log files. Use `clearSystemFiles` capability to turn on.");
        P2pChatViewControllerPageObject_Second.enterNewMessage("WTF?");
        P2pChatViewControllerPageObject_Second.sendMessageButtonClick();
        P2pChatViewControllerPageObject.checkMessageText("id:WTF?");
        P2pChatViewControllerPageObject.enterNewMessage("im fine, thanx!");
        P2pChatViewControllerPageObject.sendMessageButtonClick();
        P2pChatViewControllerPageObject_Second.checkMessageText("id:im fine, thanx!");



    }
}
