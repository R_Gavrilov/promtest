package tests;

import lib.CoreTestCase;
import lib.ui.CommonNavigationUI;
import lib.ui.ContactsMainPageObject;
import lib.ui.MessagesPageObject;
import lib.ui.factories.CommonNavigationUiFactory;
import lib.ui.factories.ContactsMainPageObjectFactory;
import lib.ui.factories.MessagesPageObjectFactory;
import org.junit.Test;

public class TestClass extends CoreTestCase {

        @Test

        public void testAddingContact() throws Exception {
                MessagesPageObject MessagesPageObject = MessagesPageObjectFactory.get(driver);
                MessagesPageObject.confirmAlerts();
                CommonNavigationUI CommonNavigationUI = CommonNavigationUiFactory.get(driver);
                CommonNavigationUI.switchToContactsTab();
                ContactsMainPageObject ContactsMainPageObject = ContactsMainPageObjectFactory.get(driver);
                ContactsMainPageObject.clickOnTheAddContactButton();
                MessagesPageObject.confirmCameraAlertAndAddContact();
                ContactsMainPageObject.giveNameToContact();

        }
}





