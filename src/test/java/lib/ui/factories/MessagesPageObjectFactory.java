package lib.ui.factories;

import io.appium.java_client.AppiumDriver;
import lib.Platform;
import lib.ui.MessagesPageObject;
import lib.ui.android.AndroidMessagesPageObject;
import lib.ui.iOS.iOSMessagesPageObject;

public class MessagesPageObjectFactory {
    public static MessagesPageObject get(AppiumDriver driver)
    {
        if(Platform.getInstance().isAndroid()) {
            return new AndroidMessagesPageObject(driver);
        } else {
            return new iOSMessagesPageObject(driver);
        }
    }
}
