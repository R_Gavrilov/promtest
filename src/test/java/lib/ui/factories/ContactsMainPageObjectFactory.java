package lib.ui.factories;

import io.appium.java_client.AppiumDriver;
import lib.Platform;
import lib.ui.ContactsMainPageObject;
import lib.ui.android.AndroidContactsMainPageObject;
import lib.ui.iOS.iOSContactsMainPageObject;

public class ContactsMainPageObjectFactory {
    public static ContactsMainPageObject get(AppiumDriver driver) {
        if (Platform.getInstance().isAndroid()) {
            return new AndroidContactsMainPageObject(driver);
        } else {
            return new iOSContactsMainPageObject(driver);
        }
    }
}
