package lib.ui.factories;

import io.appium.java_client.AppiumDriver;
import lib.Platform;
import lib.ui.P2pChatViewControllerPageObject;
import lib.ui.android.AndroidP2pChatViewControllerPageObject;
import lib.ui.iOS.iOSP2pChatViewControllerPageObject;

public class P2pChatViewControllerPageObjectFactory {
    public static P2pChatViewControllerPageObject get(AppiumDriver driver)
    {
        if(Platform.getInstance().isAndroid())
        {
            return new AndroidP2pChatViewControllerPageObject(driver);
        } else {
            return new iOSP2pChatViewControllerPageObject(driver);
        }
    }
}
