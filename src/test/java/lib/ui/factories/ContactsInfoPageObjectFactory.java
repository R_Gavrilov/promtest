package lib.ui.factories;

import io.appium.java_client.AppiumDriver;
import lib.Platform;
import lib.ui.ContactsInfoPageObject;
import lib.ui.android.AndroidContactsInfoPageObject;
import lib.ui.iOS.iOSContactsInfoPageObject;

public class ContactsInfoPageObjectFactory {
    public static ContactsInfoPageObject get(AppiumDriver driver)
    {
        if(Platform.getInstance().isAndroid()) {
            return new AndroidContactsInfoPageObject(driver);
        } else {
            return new iOSContactsInfoPageObject(driver);
        }
    }
}
