package lib.ui.factories;

import io.appium.java_client.AppiumDriver;
import lib.Platform;
import lib.ui.EditContactPageObject;
import lib.ui.android.AndroidEditContactPageObject;
import lib.ui.iOS.iOSEditContactPageObject;

public class EditContactPageObjectFactory {
    public static EditContactPageObject get(AppiumDriver driver)
    {
        if(Platform.getInstance().isAndroid()) {
            return new AndroidEditContactPageObject(driver);
        } else {
            return new iOSEditContactPageObject(driver);
        }
    }
}
