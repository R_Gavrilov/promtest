package lib.ui;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.TouchAction;
import lib.Platform;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;

abstract public class CommonNavigationUI extends MainPageObject {
    public CommonNavigationUI(AppiumDriver driver)
    {
        super(driver);
    }

    protected static String

            someElement,
            someElementForContainsMethod1,
            someElementForContainsMethod2,
            backElement,
            ziftElementTextTemplate,
            templateCellElement,
            cellElement,
            someElementIOS,
            photoSharingIOS,
            contactsTabBarElement,
            moreTabBarElement,
            messagesTabBarElement;


    public void switchToContactsTab()
    {
        this.waitForElementAndClick(contactsTabBarElement, "contactsTabBarElement not present", 40);
    }

    public void switchToEditProfileScreen()
    {
        this.waitForElementAndClick(moreTabBarElement, "moreTabBarElement not present", 40);
    }

    public void  switchToMessagesTab()
    {
        this.waitForElementAndClick(messagesTabBarElement, "moreTabBarElement not present", 40);

    }










    public void tapOnElement(String value){
        String selectedElement = selectedElement(value);
        this.waitForElementVisibilityAndClick(selectedElement, value + " element not found!", 20);
    }



    public void expandCell(String cell)
    {
        String s = String.format(templateCellElement, cell);
        cellElement = s;
        WebElement element = this.waitForElementVisibility(
                cellElement, "cellElement not found", 20);
        Dimension size = element.getSize();
        int x = element.getLocation().getX() + size.getWidth() / 2;
        int y = element.getLocation().getY() + size.getHeight() / 2;
        TouchAction action = new TouchAction(driver);
        action.tap(x, y).perform();
    }



    public void hardSwipe(int end_y)
    {
        new TouchAction(driver).press(863, 1608).waitAction(2000).moveTo(863, end_y).release().perform();
    }

    /*TEMPLATES METHODS*/
    private static String selectedElement(String value) {
        return someElement.replace("{value}", value);
    }
    private static String selectedElementIOS(String value) {
        return someElementIOS.replace("{value}", value);
    }

    /*TEMPLATES METHODS*/

    private static String ziftElement(String text)
    {
        return ziftElementTextTemplate.replace("{value}", text);
    }


    public void swipeUpToElementPresent(String value) {
        String selectedElement = selectedElement(value);
        if(Platform.getInstance().isAndroid()) {
            this.swipeUpToFindElement(selectedElement, value + " element not found!", 20);
        } else {
            this.swipeUpTillElementAppear(selectedElement, value + " element not found!", 60);
        }



    }
    public void swipeDownToElementPresent(String value) {
        String elementFromAppListTab = selectedElement(value);
        this.swipeDownToFindElement(elementFromAppListTab, value + " element not found!", 20);
    }

    public void swipeFuckingDown()
    {
        this.swipeDown(500);
    }
    public void swipeToElementNotPresentAccuracy(String value) {
        String elementFromAppListTab = selectedElement(value);
        this.swipeUpToElementNotPresentAccuracy(elementFromAppListTab, value + " element not found!", 20);
    }
    public void swipeToElementPresentAccuracy(String value) {
        String elementFromAppListTab = selectedElement(value);
        this.swipeUpToElementPresentAccuracy(elementFromAppListTab, value + " element not found!", 50);
    }

    public void swipeToElementPresentAccuracyIOS(String value) {
        String elementFromAppListTab = selectedElement(value);
        this.swipeUpTillElementAppearAccuracy(elementFromAppListTab, value + " element not found!", 50);
    }



//метод поиска по contains для iOS
    public void waitForElementPresentByTextIOS(String text)
    {
        String selectedElement = selectedElementIOS(text);
        this.waitForElementVisibility(selectedElement, text + " element not found!", 60);
    }


    // поиск по строгому соответсвию: xpath://*[@text='{value}']
    public void waitForElementPresentByText(String text)
    {
        String selectedElement = selectedElement(text);
        this.waitForElementVisibility(selectedElement, text + " element not found!", 60);
    }
    //  поиск по нестрогому соответсвию: xpath://*[contains(@text,'{value}')]
    public void waitForElementPresentByTextContains(String text)
    {
        String elementText = ziftElement(text);
        this.waitForElementPresent(elementText, "element with " + elementText +  " not found!", 60);
    }

    // составной локатор, осуществляющитй поиск по нестрогому соответствию xpath://*[contains(@text,'{value}') and contains(@text,'{value2}')]
    private static String selectedElementForContainsMethod(String text1) {
        return someElementForContainsMethod1.replace("{value}", text1);
    }
    private static String selectedElementForContainsMethod2(String text2) {
        return someElementForContainsMethod2.replace("{value2}", text2);
    }
    public void waitForElementPresentByContainsText(String text1, String text2)
    {
        String someElementForContainsMethod = selectedElementForContainsMethod(text1);
        String someElementForContainsMethod2 = selectedElementForContainsMethod2(text2);
        this.waitForElementPresent((someElementForContainsMethod + someElementForContainsMethod2),
                "element with " + someElementForContainsMethod + someElementForContainsMethod2 + " not found! ", 60);
    }




    //  так вышло
    public void waitForSpotifyDescription() {
        if (Platform.getInstance().isAndroid()) {
            try {
                this.waitForElementPresentByText("Stranger Danger");
            }
            catch (TimeoutException e) {
                this.swipeToElementPresentAccuracy("Stranger Danger");

                this.waitForElementPresentByTextContains("Photo Sharing");
                this.waitForElementPresentByText("In App Purchases");
                this.waitForElementPresentByText("Live Streaming");
                this.waitForElementPresentByText("Location Tracking");
                this.waitForElementPresentByText("Chat");
                this.waitForElementPresentByContainsText("This App doesn", "t Contain:");
                this.waitForElementPresentByContainsText(
                        "Spotify is a music streaming app that allows users to search their favorite artists",
                        "Learn About Spotify Music on the App Advisor");
                this.waitForElementPresentByText("Category: Music & Audio");
            }
        } else {

                boolean elementVisibility = this.isElementLocatedOnTheScreen(photoSharingIOS);
                if(elementVisibility == true) {
                    this.waitForElementPresentByTextContains("Photo Sharing");
                    this.waitForElementPresentByText("In App Purchases");
                    this.waitForElementPresentByText("Live Streaming");
                    this.waitForElementPresentByText("Location Tracking");
                    this.waitForElementPresentByText("Chat");
                    this.waitForElementPresentByText("This App doesn’t Contain:");
                    this.waitForElementPresentByTextIOS("Spotify is a music streaming app that");
                } else if(elementVisibility == false) {

                    this.swipeUpTillElementAppearAccuracy(photoSharingIOS, "photoSharingIOS not found!", 100);

                    this.swipeToElementPresentAccuracyIOS("Stranger Danger");
                    this.waitForElementPresentByTextContains("Photo Sharing");
                    this.waitForElementPresentByText("In App Purchases");
                    this.waitForElementPresentByText("Live Streaming");
                    this.waitForElementPresentByText("Location Tracking");
                    this.waitForElementPresentByText("Chat");
                    this.waitForElementPresentByText("This App doesn’t Contain:");
                    this.waitForElementPresentByTextIOS("Spotify is a music streaming app that");
                }

        }
    }


    //swipes for ios
    public void swipeToIOSElementQuick()
    {
        if(Platform.getInstance().isAndroid()) {
            return;
        } else {
            this.swipeUpTillElementAppearAccuracy(photoSharingIOS, "photoSharingIOS not found!", 100);
        }

    }  //хуета! вьебать тред слип на минуту нахуй (после CommonNavigationUI.waitForElementPresentByTextContains("Screentime exhausted Mode: No Internet");)
    // и помотреть завтра блядь!





}