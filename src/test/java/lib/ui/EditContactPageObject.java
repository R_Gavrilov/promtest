package lib.ui;

import io.appium.java_client.AppiumDriver;

public class EditContactPageObject extends MainPageObject {
    public EditContactPageObject(AppiumDriver driver)
    {
        super(driver);
    }
    protected static String
            showMyContactElement,
            AllKeysReadElement;

    public void showMyContactClick()
    {
        this.waitForElementVisibilityAndClick(showMyContactElement, "showMyContactElement not present", 40);
    }

    public void AllKeysReadButtonClick()
    {
        this.waitForElementAndClick(AllKeysReadElement, "showMyContactElement not present", 40);
    }





}
