package lib.ui;

import io.appium.java_client.AppiumDriver;

abstract public class MessagesPageObject extends MainPageObject {
    public MessagesPageObject(AppiumDriver driver)
    {
        super(driver);
    }

    protected static String

            notificationsAlertAllow,
            microphoneAlertAllow,
            contactsTabBarElement,
            newContactButton,
            cameraAccessButton,
            doneButton,
            backToInfoButton,
            addAttachButton,
            alertAccessPhotoButton,
            galleryButton,
            galleryElement,
            successfulDeliveryElement,
            moreTabBarElement,
            messagesTableCellElement;



    public void confirmAlerts()
    {
        this.waitForElementAndClick(notificationsAlertAllow, "notificationsAlert not present", 40);
        this.waitForElementAndClick(microphoneAlertAllow, "microphoneAlert not present", 40);
        System.out.println("confirm");
    }

    public void goToChat()
    {
        this.waitForElementAndClick(messagesTableCellElement, "messagesTableCellElement not found!", 40);

    }



    public void confirmCameraAlertAndAddContact()
    {
        this.waitForElementAndClick(cameraAccessButton, "", 40);

    }


    public void doneButtonClick()
    {
        this.waitForElementAndClick(doneButton, "", 60);
    }



    public void backToInfo()
    {
        this.waitForElementAndClick(backToInfoButton, "", 20);
    }

    public void addAttachButtonClick()
    {
        this.waitForElementAndClick(addAttachButton, "",30);
    }

    public void confirmAlertAccessPhoto()
    {
        this.waitForElementAndClick(alertAccessPhotoButton, "",30);
    }

    public void galleryButtonclick()
    {
        this.waitForElementAndClick(galleryButton, "",30);
    }

    public void selectGalleryElement()
    {
        this.waitForElementVisibilityAndClick(galleryElement, "",30);
    }

    public void waitUntilTheAttachIsDelivered()
    {
        this.waitForElementPresent(successfulDeliveryElement, "",60);

    }


    public void waitForSendingMessage()
    {

    }







}
