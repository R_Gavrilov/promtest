package lib.ui.android;

import io.appium.java_client.AppiumDriver;
import lib.ui.MessagesPageObject;

public class AndroidMessagesPageObject extends MessagesPageObject {
    public AndroidMessagesPageObject(AppiumDriver driver)
    {
        super(driver);
    }

    static {
        //XCUIElementTypeOther[3]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[3]
        notificationsAlertAllow = "id:Allow";

        //XCUIElementTypeOther[3]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[3]
        microphoneAlertAllow = "id:OK";

        //XCUIElementTypeButton[@name="Contacts"]
        contactsTabBarElement = "id:Contacts";

        //XCUIElementTypeButton[@name="NEW CONTACT BUTTON"]
        newContactButton = "id:NEW CONTACT BUTTON";

        //XCUIElementTypeButton[@name="OK"]
        cameraAccessButton = "id:OK";





    }

}
