package lib.ui.android;

import io.appium.java_client.AppiumDriver;
import lib.ui.ContactsMainPageObject;

public class AndroidContactsMainPageObject extends ContactsMainPageObject {
    public AndroidContactsMainPageObject(AppiumDriver driver)
    {
        super(driver);
    }
}
