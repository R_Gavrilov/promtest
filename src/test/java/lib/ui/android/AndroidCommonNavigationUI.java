package lib.ui.android;

import io.appium.java_client.AppiumDriver;
import lib.ui.CommonNavigationUI;

public class AndroidCommonNavigationUI extends CommonNavigationUI {
    public AndroidCommonNavigationUI(AppiumDriver driver) {
        super(driver);
    }

    static {
                someElement = "xpath://*[contains(@text,'{value}')]";
                someElementForContainsMethod1 = "xpath://*[contains(@text,'{value}')";
                someElementForContainsMethod2 = " and contains(@text,'{value2}')]";
                backElement = "id:com.contentwatch.ghoti.cp2.parent:id/back";
                ziftElementTextTemplate = "xpath://*[contains(@text,'{value}')]";
                cellElement = "";
                templateCellElement = "xpath://*[@text='%s']";
    }


}
