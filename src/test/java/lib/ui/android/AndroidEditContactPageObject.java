package lib.ui.android;

import io.appium.java_client.AppiumDriver;
import lib.ui.EditContactPageObject;

public class AndroidEditContactPageObject extends EditContactPageObject {
    public AndroidEditContactPageObject(AppiumDriver driver)
    {
        super(driver);
    }

    static {

        showMyContactElement = "id:Show my contact";
    }
}
