package lib.ui;

import io.appium.java_client.AppiumDriver;

public class P2pChatViewControllerPageObject extends MainPageObject {
    public P2pChatViewControllerPageObject(AppiumDriver driver)
    {
        super(driver);
    }
    protected static String

            messageInputField,
            sendMessageButton,
            sendedMessage;



    public void enterNewMessage(String messageText)
    {
        this.waitForElementAndSendKeys(messageInputField, messageText, "messageInputField not present!", 60);
    }

    public void sendMessageButtonClick()
    {
        this.waitForElementAndClick(sendMessageButton, "sendMessageButton not present", 60);
    }

    public void checkMessageText(String messageText)
    {
        this.waitForElementVisibility(messageText, "sendedMessage not present", 20);

    }




}
