package lib.ui;

import io.appium.java_client.AppiumDriver;

public class ContactsInfoPageObject extends MainPageObject {
    public ContactsInfoPageObject(AppiumDriver driver)
    {
        super(driver);
    }

    protected static String
        sendMessageElement;

    public void sendMessageButtonClick()
    {
        this.waitForElementAndClick(sendMessageElement, "sendMessageButton", 60);
    }


}
