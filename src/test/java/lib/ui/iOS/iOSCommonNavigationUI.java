package lib.ui.iOS;

import io.appium.java_client.AppiumDriver;
import lib.ui.CommonNavigationUI;

public class iOSCommonNavigationUI extends CommonNavigationUI {
    public iOSCommonNavigationUI(AppiumDriver driver)
    {
        super(driver);
    }

    static {
        someElement = "id:{value}";
        someElementIOS = "xpath://*[contains(@name,'{value}')]";
        backElement = "id:com.contentwatch.ghoti.cp2.parent:id/back";
        ziftElementTextTemplate = "xpath://*[contains(@name,'{value}')]";
        cellElement = "";
        templateCellElement = "xpath://*[@name='%s']";
        photoSharingIOS = "id:YouTube";

        //XCUIElementTypeButton[@name="Contacts"]
        contactsTabBarElement = "id:Contacts";

        moreTabBarElement = "id:More";

        messagesTabBarElement = "id:Messages";

    }

}
