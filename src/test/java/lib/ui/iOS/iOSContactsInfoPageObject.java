package lib.ui.iOS;

import io.appium.java_client.AppiumDriver;
import lib.ui.ContactsInfoPageObject;

public class iOSContactsInfoPageObject extends ContactsInfoPageObject {
    public iOSContactsInfoPageObject(AppiumDriver driver)
    {
        super(driver);
    }

    static {

        sendMessageElement = "id:Send message";


    }
}
