package lib.ui.iOS;

import io.appium.java_client.AppiumDriver;
import lib.ui.EditContactPageObject;

public class iOSEditContactPageObject extends EditContactPageObject {
    public iOSEditContactPageObject(AppiumDriver driver)
    {
        super(driver);
    }

    static {

        showMyContactElement = "id:Show my contact";
        AllKeysReadElement = "id:All keys read";
    }
}
