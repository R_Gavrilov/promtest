package lib.ui.iOS;

import io.appium.java_client.AppiumDriver;
import lib.ui.P2pChatViewControllerPageObject;

public class iOSP2pChatViewControllerPageObject extends P2pChatViewControllerPageObject {
    public iOSP2pChatViewControllerPageObject(AppiumDriver driver)
    {
        super(driver);
    }

    static {
                messageInputField = "xpath://XCUIElementTypeTextView";
                sendMessageButton = "id:SEND BUTTON";
                sendedMessage = "id:Test12345";

    }
}
