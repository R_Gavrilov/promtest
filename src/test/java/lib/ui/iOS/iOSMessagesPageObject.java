package lib.ui.iOS;


import io.appium.java_client.AppiumDriver;
import lib.ui.MessagesPageObject;

public class iOSMessagesPageObject extends MessagesPageObject {
    public iOSMessagesPageObject(AppiumDriver driver)
    {
        super(driver);
    }

    static {
        //XCUIElementTypeOther[3]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[3]
        notificationsAlertAllow = "id:Allow";

        //XCUIElementTypeOther[3]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[3]
        microphoneAlertAllow = "id:OK";

        //XCUIElementTypeButton[@name="Contacts"]
        contactsTabBarElement = "id:Contacts";

        //XCUIElementTypeButton[@name="NEW CONTACT BUTTON"]
        newContactButton = "id:NEW CONTACT BUTTON";

        //XCUIElementTypeButton[@name="OK"]
        cameraAccessButton = "id:OK";

        //XCUIElementTypeButton[@name="Done"]
        doneButton = "id:Done";

        backToInfoButton = "id:Info";

        addAttachButton = "id:STAPLE";

        alertAccessPhotoButton = "id:OK";

        galleryButton = "id:Gallery";

        galleryElement = "xpath://XCUIElementTypeCollectionView/XCUIElementTypeCell[2]/XCUIElementTypeOther";

        successfulDeliveryElement = "id:DarkTheme/ChatView/cell_video_play";

        moreTabBarElement = "id:More";

        messagesTableCellElement = "xpath://XCUIElementTypeTable/XCUIElementTypeCell";





    }


}
