package lib.ui.iOS;

import io.appium.java_client.AppiumDriver;
import lib.ui.ContactsMainPageObject;

public class iOSContactsMainPageObject extends ContactsMainPageObject {
    public iOSContactsMainPageObject(AppiumDriver driver)
    {
        super(driver);
    }

    static {

        //XCUIElementTypeButton[@name="NEW CONTACT BUTTON"]
        newContactButton = "id:NEW CONTACT BUTTON";
        setAvatarButton = "id:DarkTheme/Settings/SET_AVATAR_BUTTON";
        textField = "xpath://XCUIElementTypeTextField";

        //XCUIElementTypeButton[@name="Done"]
        doneButton = "id:Done";

        cameraAccessButton = "id:OK";




    }
}
