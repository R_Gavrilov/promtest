package lib.ui;

import io.appium.java_client.AppiumDriver;

public class ContactsMainPageObject extends MainPageObject {
    public ContactsMainPageObject(AppiumDriver driver)
    {
        super(driver);
    }
    protected static String
            newContactButton,
            cameraAccessButton,
            setAvatarButton,
            textField,
            doneButton;

    public void clickOnTheAddContactButton()
    {
        this.waitForElementAndClick(newContactButton, "newContactButton not present!", 30);
    }

    public void confirmCameraAlertAndAddContact()
    {
        this.waitForElementAndClick(cameraAccessButton, "cameraAccessButton not present or QR-code not read ", 40);
    }

    public void giveNameToContact()
    {
        this.waitForElementPresent(setAvatarButton, "", 60);
        this.waitForElementAndSendKeys(textField, "Steven", "", 60);
    }

    public void doneButtonClick()
    {
        this.waitForElementAndClick(doneButton, "", 60);
    }


}
