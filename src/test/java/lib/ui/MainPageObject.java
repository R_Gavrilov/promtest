package lib.ui;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileBy;
import io.appium.java_client.TouchAction;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;
import java.util.regex.Pattern;


public class MainPageObject {

    protected AppiumDriver driver;

    public MainPageObject(AppiumDriver driver)
    {
        this.driver = driver;
    }


/* Старая реализация ожидания braze - ивента (связано с class CommonNavigationUI/public void keepZiftFree())

   public boolean waitForBrazeEvent(String locator)
    {
        By by = this.getLocatorByString(locator);
        Boolean isPresent = driver.findElements(by).size() > 0;
        return isPresent;
    }*/




    public boolean waitForElementNotPresent(String locator, String errorMessage, long timeOutInSeconds) {
        By by = this.getLocatorByString(locator);
        WebDriverWait wait = new WebDriverWait(driver, timeOutInSeconds);
        wait.withMessage(errorMessage + "\n");
        return wait.until(ExpectedConditions.invisibilityOfElementLocated(by));
    }



    public boolean waitForSelectedAttributeElementTrue(String locator, String errorMessage, long timeOutInSeconds) {
        By by = this.getLocatorByString(locator);
        WebDriverWait wait = new WebDriverWait(driver, timeOutInSeconds);
        wait.withMessage(errorMessage + "\n");
        return wait.until(ExpectedConditions.attributeToBe(by, "selected", "true"));
    }

    public boolean waitForUnlimitedCheckboxAttributeTrue(String locator, String errorMessage, long timeOutInSeconds) {
        By by = this.getLocatorByString(locator);
        WebDriverWait wait = new WebDriverWait(driver, timeOutInSeconds);
        wait.withMessage(errorMessage + "\n");
        return wait.until(ExpectedConditions.attributeToBe(by, "checked", "true"));
    }

    public boolean waitForUnlimitedCheckboxAttributeFalse(String locator, String errorMessage, long timeOutInSeconds) {
        By by = this.getLocatorByString(locator);
        WebDriverWait wait = new WebDriverWait(driver, timeOutInSeconds);
        wait.withMessage(errorMessage + "\n");
        return wait.until(ExpectedConditions.attributeToBe(by, "checked", "false"));
    }

    public boolean waitForElementAttributeEnabledTrue(String locator, String errorMessage, long timeOutInSeconds) {
        By by = this.getLocatorByString(locator);
        WebDriverWait wait = new WebDriverWait(driver, timeOutInSeconds);
        wait.withMessage(errorMessage + "\n");
        return wait.until(ExpectedConditions.attributeToBe(by, "enabled", "true"));
    }

    public boolean waitForElementAttributeEnabledFalse(String locator, String errorMessage, long timeOutInSeconds) {
        By by = this.getLocatorByString(locator);
        WebDriverWait wait = new WebDriverWait(driver, timeOutInSeconds);
        wait.withMessage(errorMessage + "\n");
        return wait.until(ExpectedConditions.attributeToBe(by, "enabled", "false"));
    }


    public WebElement waitForElementPresent(String locator, String errorMessage, long timeoutInSeconds) {
        By by = this.getLocatorByString(locator);
        WebDriverWait wait = new WebDriverWait(driver, timeoutInSeconds);
        wait.withMessage(errorMessage + "\n");
        return wait.until(ExpectedConditions.presenceOfElementLocated(by));
    }

    public WebElement waitForElementPresent(String locator, String errorMessage) {
        return waitForElementVisibility(locator, errorMessage, 20);

    }
    public WebElement waitForElementAndClick(String locator, String errorMessage, long timeoutInSeconds)
    {
        WebElement element = waitForElementVisibility(locator, errorMessage, timeoutInSeconds);
        element.click();
        return element;

    }

    public WebElement waitForElementAndClick(String locator, String errorMessage) {
        return waitForElementAndClick(locator, errorMessage, 20);

    }






    public WebElement waitForElementVisibility(String locator, String errorMessage, long timeoutInSeconds) {
        By by = this.getLocatorByString(locator);
        WebDriverWait wait = new WebDriverWait(driver, timeoutInSeconds);
        wait.withMessage(errorMessage + "\n");
        return wait.until(ExpectedConditions.visibilityOfElementLocated(by));
    }

    public WebElement waitForElementVisibilityAndClick(String locator, String errorMessage, long timeoutInSeconds) {
        WebElement element = waitForElementVisibility(locator, errorMessage, timeoutInSeconds);
        element.click();
        return element;

    }







    public WebElement waitForElementAndSendKeys(String locator, String value, String errorMessage, long timeoutInSeconds) {
        WebElement element = waitForElementPresent(locator, errorMessage, timeoutInSeconds);
        element.sendKeys(value);
        return element;

    }

    public WebElement waitForElementAndSendKeys(String locator, String value, String errorMessage) {
        return waitForElementAndSendKeys(locator, value, errorMessage,  20);
    }



// swipeAccuracy

    public void swipeUpAccuracy(int timeOfSwipe) {
        TouchAction action = new TouchAction(driver);
        Dimension size = driver.manage().window().getSize();
        int x = size.width / 2;
        int start_y = (int) (size.height * 0.8);
        int end_y = (int) (size.height * 0.78);

        action.press(x, start_y).waitAction(timeOfSwipe).moveTo(x, end_y).release().perform();
    }



    public void swipeUpQuickAccuracy() {
        swipeUpAccuracy(500);
    }

    public void swipeUpToFindElementAccuracy(String locator, String errorMessage, int maxSwipes) {
        By by = this.getLocatorByString(locator);
        int alreadySwiped = 0;
        while (driver.findElements(by).size() == 0) {
            if (alreadySwiped > maxSwipes) {
                waitForElementPresent(locator, "Cannot find element by swiping up. \n" + errorMessage, 0);
                return;
            }

            swipeUpQuickAccuracy();
            ++alreadySwiped;
        }
    }

    public void swipeUpToElementNotPresentAccuracy(String locator, String errorMessage, int maxSwipes){
        By by = this.getLocatorByString(locator);
        int alreadySwiped = 0;
        while (driver.findElements(by).size() == 1) {
            if (alreadySwiped > maxSwipes) {
                waitForElementNotPresent(locator, "Cannot swipeUpToElementNotPresentAccuracy. \n" + errorMessage, 0);
                return;
            }

            swipeUpQuickAccuracy();
            ++alreadySwiped;
        }
    }

    public void swipeUpToElementPresentAccuracy(String locator, String errorMessage, int maxSwipes){
        By by = this.getLocatorByString(locator);
        int alreadySwiped = 0;
        while (driver.findElements(by).size() == 0) {
            if (alreadySwiped > maxSwipes) {
                waitForElementVisibility(locator, "Cannot swipeUpToElementNotPresentAccuracy. \n" + errorMessage, 0);
                return;
            }

            swipeUpQuickAccuracy();
            ++alreadySwiped;
        }
    }

//swipe up

    public void swipeUp(int timeOfSwipe) {
        TouchAction action = new TouchAction(driver);
        Dimension size = driver.manage().window().getSize();
        int x = size.width / 2;
        int start_y = (int) (size.height * 0.8);
        int end_y = (int) (size.height * 0.55);

        action.press(x, start_y).waitAction(timeOfSwipe).moveTo(x, end_y).release().perform();
    }

    public void swipeDown(int timeOfSwipe) {
        TouchAction action = new TouchAction(driver);
        Dimension size = driver.manage().window().getSize();
        int x = size.width / 2;
        int start_y = (int) (size.height * 0.3);
        int end_y = (int) (size.height * 0.55);

        action.press(x, start_y).waitAction(timeOfSwipe).moveTo(x, end_y).release().perform();
    }


    public void swipeUpQuick() {
        swipeUp(500);
    }

    public void swipeUpToFindElement(String locator, String errorMessage, int maxSwipes) {
        By by = this.getLocatorByString(locator);
        int alreadySwiped = 0;
        while (driver.findElements(by).size() == 0) {
            if (alreadySwiped > maxSwipes) {
                waitForElementPresent(locator, "Cannot find element by swiping up. \n" + errorMessage, 0);
                return;
            }

            swipeUpQuick();
            ++alreadySwiped;
        }
    }

    //swipe for iOS
    public boolean isElementLocatedOnTheScreen(String locator)
    {
        int element_location_by_y = this.waitForElementPresent(
                locator, "Cannot find element by locator", 2).getLocation().getY();
        int screen_size_by_y = driver.manage().window().getSize().getHeight();
        return element_location_by_y < screen_size_by_y;
    }
    public void swipeUpAccuracyForIOS(int timeOfSwipe) {
        TouchAction action = new TouchAction(driver);
        Dimension size = driver.manage().window().getSize();
        int x = size.width / 2;
        int start_y = (int) (size.height * 0.8);
        int end_y = (int) (size.height * 0.75);

        action.press(x, start_y).waitAction(timeOfSwipe).moveTo(x, end_y).release().perform();
    }
    public void swipeUpQuickAccuracyForIOS() {
        swipeUpAccuracyForIOS(500);
    }
    public void swipeUpTillElementAppear(String locator, String errorMessage, int maxSwipes)
    {
        int alreadySwiped = 0;

        while (!this.isElementLocatedOnTheScreen(locator))
        {
            if (alreadySwiped > maxSwipes){
                Assert.assertTrue(errorMessage, this.isElementLocatedOnTheScreen(locator));
            }

            swipeUpQuick();
            ++alreadySwiped;
        }

    }

    public void swipeUpTillElementAppearAccuracy(String locator, String errorMessage, int maxSwipes)
    {
        int alreadySwiped = 0;

        while (!this.isElementLocatedOnTheScreen(locator))
        {
            if (alreadySwiped > maxSwipes){
                Assert.assertTrue(errorMessage, this.isElementLocatedOnTheScreen(locator));
            }
            swipeUpQuickAccuracyForIOS();
            ++alreadySwiped;
        }

    }


    public void swipeDownTillElementAppear(String locator, String errorMessage, int maxSwipes) {
        int alreadySwiped = 0;

        while (!this.isElementLocatedOnTheScreen(locator))
        {
            if (alreadySwiped > maxSwipes){
                Assert.assertTrue(errorMessage, this.isElementLocatedOnTheScreen(locator));
            }

            swipeDownQuick();
            ++alreadySwiped;
        }
    }





    public void swipeUpToElementNotPresent(String locator, String errorMessage, int maxSwipes){
        By by = this.getLocatorByString(locator);
        int alreadySwiped = 0;
        while (driver.findElements(by).size() == 1) {
            if (alreadySwiped > maxSwipes) {
                waitForElementNotPresent(locator, "Cannot swipeUpToElementNotPresentAccuracy. \n" + errorMessage, 1);
                return;
            }

            swipeUpQuick();
            ++alreadySwiped;
        }
    }

    //swipe down
/*
    public void swipeDown(int timeOfSwipe) {
        TouchAction action = new TouchAction(driver);
        Dimension size = driver.manage().window().getSize();
        int x = size.width / 2;
        int start_y = (int) (size.height * 0.2);
        int end_y = (int) (size.height * 0.45);

        action.press(x, start_y).waitAction(timeOfSwipe).moveTo(x, end_y).release().perform();
    }*/

    public void swipeDownQuick() {
        swipeDown(500);
    }

    public void swipeDownToFindElement(String locator, String errorMessage, int maxSwipes) {
        By by = this.getLocatorByString(locator);
        int alreadySwiped = 0;
        while (driver.findElements(by).size() == 0) {
            if (alreadySwiped > maxSwipes) {
                waitForElementPresent(locator, "Cannot find element by swiping up. \n" + errorMessage, 1);
                return;
            }

            swipeDownQuick();
            ++alreadySwiped;
        }
    }

    public int getAmountOfElements(String locator)
    {
        By by = this.getLocatorByString(locator);
        List elements = driver.findElements(by);
        return  elements.size();
    }

    public List findElements(String locator)
    {
        By by = this.getLocatorByString(locator);
        List geg = driver.findElements(by);
        return geg;
    }


    public void waitForElementAndClear (String locator, String errorMessage, long timeoutInSeconds)
    {
        WebElement element = waitForElementPresent(locator, errorMessage, timeoutInSeconds);
        element.clear();
        return;
    }





    public void swipeElementToUp(String locator, String errorMessage)
    {
        WebElement element = waitForElementPresent(locator, errorMessage, 20);

        int upper_y = element.getLocation().getY();
        int lower_y = upper_y + element.getSize().getHeight();
        int left_x = element.getLocation().getX();
        int right_x = left_x + element.getSize().getWidth();
        int middle_x = (left_x + right_x) / 2;

        TouchAction action = new TouchAction(driver);
        action.press(middle_x, lower_y).waitAction(500).moveTo(middle_x, upper_y).release().perform();

    }




    public boolean scrollToElementById(String elemId) {
        try {
            driver.findElement(MobileBy.AndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true).instance(0))" +
                    ".scrollIntoView(new UiSelector().resourceId(\" + elemId + \").instance(0))"));
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Scroll to element failed");
            return false;
        }
    }

    private By getLocatorByString(String locatorWithType)
    {
        String[] explodedLocator = locatorWithType.split(Pattern.quote(":"), 2);
        String byType = explodedLocator[0];
        String locator = explodedLocator[1];

        if (byType.equals("xpath")) {
            return By.xpath(locator);
        } else if (byType.equals("id")){
            return By.id(locator);
        } else {
            throw new IllegalArgumentException("Cannot get type of locator: " + locatorWithType);

        }

    }






}





/*    public void swipeSpinner(By by, int timeOfSwipe) {

        TouchAction action1 = new TouchAction(driver);

        Dimension size = driver.findElement(by).getSize();

        int x = size.width / 2;
        int start_y = (int) (size.height * 0.8);
        int end_y = (int) (size.height * 0.2);

        action1.press(x, start_y).waitAction(timeOfSwipe).moveTo(x, end_y).release().perform();





       protected void swipeUpToElementNotPresentAccuracy(By by, String errorMessage, int maxSwipes) {
        int alreadySwiped = 0;

        while (driver.findElements(by).size() == 1) {

            int amount = driver.findElements(by).size();
            System.out.println("Amount of elements: " + amount);
            System.out.println("Already swiped: " + alreadySwiped);

            if (amount != 1) {
                try{Thread.sleep(2000);}catch(Exception e){}
            }

            if (alreadySwiped > maxSwipes) {
                waitForElementNotPresent(by, "Cannot swipeUpToElementNotPresentAccuracy. \n" + errorMessage, 0);
                return;
            }

            swipeUpQuickAccuracy();
            ++alreadySwiped;
        }
    }*/
