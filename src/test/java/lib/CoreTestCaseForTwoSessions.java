package lib;

import io.appium.java_client.AppiumDriver;
import junit.framework.TestCase;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.util.HashMap;

public class CoreTestCaseForTwoSessions extends TestCase {

    protected AppiumDriver driver;
    protected AppiumDriver driver_second;

    @Override
    protected void setUp() throws Exception
    {
        super.setUp();

        HashMap data = new HashMap<String, String>();

//        data.put("deviceName", "iPhone 6s");
//        data.put("udid", "3355d0aea44873bb196e628359528030d8574d5c");
//        data.put("platformVersion", "11.3");
//        data.put("updatedWDABundleId", "EscTech.zifttest");

        data.put("deviceName", "iPhone 7 Plus");
        data.put("udid", "b305f209ce11d6219f706735975ccb69c831aa25");
        data.put("platformVersion", "11.4");
        data.put("updatedWDABundleId", "EscTech.zifttest");


        driver = Platform.getInstance().getDriver(Platform.APPIUM_URL, data);

        HashMap data_second = new HashMap<String, String>();
//        data.put("deviceName", "iPhone 6s");
//        data.put("udid", "3355d0aea44873bb196e628359528030d8574d5c");
//        data.put("platformVersion", "11.3");
//        data.put("updatedWDABundleId", "EscTech.zifttest");

        data_second.put("deviceName", "iPhone SE White");
        data_second.put("udid", "249dc806cf4028b81873a882d8f0ab73fb212db2");
        data_second.put("platformVersion", "11.1.2");
        data_second.put("updatedWDABundleId", "EscTech.zifttest");



        driver_second = Platform.getInstance().getDriver(Platform.APPIUM_URL_SECOND, data_second);









/*      HashMap data = new HashMap<String, String>();
        data.put("deviceName", "iPhone 5s");
        data.put("updatedWDABundleId", "EscTech.zifttest");
        driver = Platform.getInstance().getDriver(Platform.APPIUM_URL, data);


        HashMap data_second = new HashMap<String, String>();
        data_second.put("deviceName", "iPhone SE");
        data_second.put("updatedWDABundleId", "EscTech.zifttest");

        driver_second = Platform.getInstance().getDriver(Platform.APPIUM_URL_SECOND, data_second);*/

    }

    @Override
    protected void tearDown() throws Exception {
        driver.quit();
        driver_second.quit();
        super.tearDown();
    }


}


// appium -p 4726 --default-capabilities '{"wdaLocalPort": "8646", "appWaitDuration": "60000", "deviceReadyTimeout": "500", "launchTimeout": "100000", "newCommandTimeout": "500"}'

// appium -p 4727 --default-capabilities '{"wdaLocalPort": "7397", "appWaitDuration": "60000", "deviceReadyTimeout": "500", "launchTimeout": "100000", "newCommandTimeout": "500"}'