package lib;

import io.appium.java_client.AppiumDriver;
import junit.framework.TestCase;
import org.openqa.selenium.ScreenOrientation;

import java.util.HashMap;

//Используется, если не нужны два драйвера
public class CoreTestCase extends TestCase {

    protected AppiumDriver driver;

    @Override
    protected void setUp() throws Exception {

        super.setUp();
        HashMap data = new HashMap<String, String>();
        data.put("deviceName", "iPhone SE White");
        data.put("udid", "249dc806cf4028b81873a882d8f0ab73fb212db2");
        data.put("platformVersion", "11.1.2");
        data.put("updatedWDABundleId", "EscTech.zifttest");
        driver = Platform.getInstance().getDriver(Platform.APPIUM_URL, data);
        this.rotateScreenPortrait();

    }

    @Override
    protected void tearDown() throws Exception
    {
        driver.quit();
        super.tearDown();
    }





    protected void rotateScreenPortrait()

    {
        driver.rotate(ScreenOrientation.PORTRAIT);
    }

    protected void rotateScreenLandscape()

    {
        driver.rotate(ScreenOrientation.LANDSCAPE);
    }

    protected void backgroundApp(int seconds)
    {
        driver.runAppInBackground(seconds);
    }

    protected void hideKeyboard()

    {
        driver.hideKeyboard();
    }

    protected void navigateBack()
    {
        driver.navigate().back();
    }


}










